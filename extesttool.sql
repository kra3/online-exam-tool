-- MySQL dump 10.13  Distrib 5.5.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: extesttool
-- ------------------------------------------------------
-- Server version	5.5.24-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_bda51c3c` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `group_id_refs_id_3cea63fe` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_a7792de1` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_728de91f` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add site',6,'add_site'),(17,'Can change site',6,'change_site'),(18,'Can delete site',6,'delete_site'),(19,'Can add college',7,'add_college'),(20,'Can change college',7,'change_college'),(21,'Can delete college',7,'delete_college'),(22,'Can add student',8,'add_student'),(23,'Can change student',8,'change_student'),(24,'Can delete student',8,'delete_student'),(25,'Can add question paper',9,'add_questionpaper'),(26,'Can change question paper',9,'change_questionpaper'),(27,'Can delete question paper',9,'delete_questionpaper'),(28,'Can add question item',10,'add_questionitem'),(29,'Can change question item',10,'change_questionitem'),(30,'Can delete question item',10,'delete_questionitem'),(31,'Can add answer item',11,'add_answeritem'),(32,'Can change answer item',11,'change_answeritem'),(33,'Can delete answer item',11,'delete_answeritem'),(34,'Can add category',12,'add_category'),(35,'Can change category',12,'change_category'),(36,'Can delete category',12,'delete_category'),(37,'Can add sub category',13,'add_subcategory'),(38,'Can change sub category',13,'change_subcategory'),(39,'Can delete sub category',13,'delete_subcategory'),(40,'Can add option',14,'add_option'),(41,'Can change option',14,'change_option'),(42,'Can delete option',14,'delete_option'),(43,'Can add categorized question',15,'add_categorizedquestion'),(44,'Can change categorized question',15,'change_categorizedquestion'),(45,'Can delete categorized question',15,'delete_categorizedquestion'),(46,'Can add multi question',16,'add_multiquestion'),(47,'Can change multi question',16,'change_multiquestion'),(48,'Can delete multi question',16,'delete_multiquestion'),(49,'Can add sub question',17,'add_subquestion'),(50,'Can change sub question',17,'change_subquestion'),(51,'Can delete sub question',17,'delete_subquestion'),(52,'Can add Data Interpretation Question',16,'add_diquestion'),(53,'Can change Data Interpretation Question',16,'change_diquestion'),(54,'Can delete Data Interpretation Question',16,'delete_diquestion'),(55,'Can add Reading Comprehension Question',16,'add_rcquestion'),(56,'Can change Reading Comprehension Question',16,'change_rcquestion'),(57,'Can delete Reading Comprehension Question',16,'delete_rcquestion'),(58,'Can add Mathematics Question',15,'add_mathquestion'),(59,'Can change Mathematics Question',15,'change_mathquestion'),(60,'Can delete Mathematics Question',15,'delete_mathquestion'),(61,'Can add Logical Reasoning Question',15,'add_logicalquestion'),(62,'Can change Logical Reasoning Question',15,'change_logicalquestion'),(63,'Can delete Logical Reasoning Question',15,'delete_logicalquestion'),(64,'Can add report',22,'add_report'),(65,'Can change report',22,'change_report'),(66,'Can delete report',22,'delete_report'),(67,'Can add log entry',23,'add_logentry'),(68,'Can change log entry',23,'change_logentry'),(69,'Can delete log entry',23,'delete_logentry');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'admin','','','arun.rajeevan@expicient.com','pbkdf2_sha256$10000$2B8bLXzCCYIB$lGgtYqcPJ4kzr2wVOzRw9gpy0ho+TLbUSGCmctwpni8=',1,1,1,'2012-09-03 13:29:08','2012-09-03 07:35:15');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_fbfc09f1` (`user_id`),
  KEY `auth_user_groups_bda51c3c` (`group_id`),
  CONSTRAINT `user_id_refs_id_831107f1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `group_id_refs_id_f0ee9890` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_fbfc09f1` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `user_id_refs_id_f2045483` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `permission_id_refs_id_67e79cb` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_fbfc09f1` (`user_id`),
  KEY `django_admin_log_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_288599e6` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c8665aa` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2012-09-03 13:28:19',1,7,'1','Test1',1,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'site','sites','site'),(7,'college','onlinetest','college'),(8,'student','onlinetest','student'),(9,'question paper','onlinetest','questionpaper'),(10,'question item','onlinetest','questionitem'),(11,'answer item','onlinetest','answeritem'),(12,'category','questionbank','category'),(13,'sub category','questionbank','subcategory'),(14,'option','questionbank','option'),(15,'categorized question','questionbank','categorizedquestion'),(16,'multi question','questionbank','multiquestion'),(17,'sub question','questionbank','subquestion'),(18,'Data Interpretation Question','questionbank','diquestion'),(19,'Mathematics Question','questionbank','mathquestion'),(20,'Logical Reasoning Question','questionbank','logicalquestion'),(21,'Reading Comprehension Question','questionbank','rcquestion'),(22,'report','reports','report'),(23,'log entry','admin','logentry');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_c25c2c28` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('eed49e88b88e67e840d5ff23fe10cb1e','OGViMzU3MGRiNjI2NjMyMWIxZmYyMDU1ODA2YThjOWUyMWIwNWExOTqAAn1xAS4=\n','2012-09-17 13:41:46');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlinetest_answeritem`
--

DROP TABLE IF EXISTS `onlinetest_answeritem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `onlinetest_answeritem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `question_item_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `onlinetest_answeritem_42ff452e` (`student_id`),
  KEY `onlinetest_answeritem_e4432b7d` (`question_item_id`),
  KEY `onlinetest_answeritem_d36b8b30` (`answer_id`),
  CONSTRAINT `answer_id_refs_id_54541672` FOREIGN KEY (`answer_id`) REFERENCES `questionbank_option` (`id`),
  CONSTRAINT `question_item_id_refs_id_fbf956df` FOREIGN KEY (`question_item_id`) REFERENCES `onlinetest_questionitem` (`id`),
  CONSTRAINT `student_id_refs_id_2622c818` FOREIGN KEY (`student_id`) REFERENCES `onlinetest_student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlinetest_answeritem`
--

LOCK TABLES `onlinetest_answeritem` WRITE;
/*!40000 ALTER TABLE `onlinetest_answeritem` DISABLE KEYS */;
/*!40000 ALTER TABLE `onlinetest_answeritem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlinetest_college`
--

DROP TABLE IF EXISTS `onlinetest_college`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `onlinetest_college` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `street` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `contact_no` varchar(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlinetest_college`
--

LOCK TABLES `onlinetest_college` WRITE;
/*!40000 ALTER TABLE `onlinetest_college` DISABLE KEYS */;
INSERT INTO `onlinetest_college` VALUES (1,'Test1','Bommanahalli','Bangalore','Karnataka','9739986363');
/*!40000 ALTER TABLE `onlinetest_college` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlinetest_questionitem`
--

DROP TABLE IF EXISTS `onlinetest_questionitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `onlinetest_questionitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_paper_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `question_item_id` int(11) DEFAULT NULL,
  `answer_id` int(11) NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  `content_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `onlinetest_questionitem_6b1a8755` (`question_paper_id`),
  KEY `onlinetest_questionitem_42dc49bc` (`category_id`),
  KEY `onlinetest_questionitem_e4432b7d` (`question_item_id`),
  KEY `onlinetest_questionitem_d36b8b30` (`answer_id`),
  KEY `onlinetest_questionitem_e4470c6e` (`content_type_id`),
  CONSTRAINT `question_item_id_refs_id_9d3b1a62` FOREIGN KEY (`question_item_id`) REFERENCES `questionbank_subquestion` (`id`),
  CONSTRAINT `answer_id_refs_id_1c70c236` FOREIGN KEY (`answer_id`) REFERENCES `questionbank_option` (`id`),
  CONSTRAINT `category_id_refs_id_4212a5cf` FOREIGN KEY (`category_id`) REFERENCES `questionbank_category` (`id`),
  CONSTRAINT `content_type_id_refs_id_96007396` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `question_paper_id_refs_id_724ed58d` FOREIGN KEY (`question_paper_id`) REFERENCES `onlinetest_questionpaper` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlinetest_questionitem`
--

LOCK TABLES `onlinetest_questionitem` WRITE;
/*!40000 ALTER TABLE `onlinetest_questionitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `onlinetest_questionitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlinetest_questionpaper`
--

DROP TABLE IF EXISTS `onlinetest_questionpaper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `onlinetest_questionpaper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `college_id` int(11) NOT NULL,
  `duration` smallint(5) unsigned NOT NULL,
  `deployed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `onlinetest_questionpaper_f92d1f50` (`college_id`),
  CONSTRAINT `college_id_refs_id_b1e0c17e` FOREIGN KEY (`college_id`) REFERENCES `onlinetest_college` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlinetest_questionpaper`
--

LOCK TABLES `onlinetest_questionpaper` WRITE;
/*!40000 ALTER TABLE `onlinetest_questionpaper` DISABLE KEYS */;
/*!40000 ALTER TABLE `onlinetest_questionpaper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `onlinetest_student`
--

DROP TABLE IF EXISTS `onlinetest_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `onlinetest_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `college_id` int(11) NOT NULL,
  `question_paper_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `register_no` int(11) NOT NULL,
  `branch` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone_no` varchar(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `onlinetest_student_f92d1f50` (`college_id`),
  KEY `onlinetest_student_6b1a8755` (`question_paper_id`),
  CONSTRAINT `question_paper_id_refs_id_7ed4b4a2` FOREIGN KEY (`question_paper_id`) REFERENCES `onlinetest_questionpaper` (`id`),
  CONSTRAINT `college_id_refs_id_ea3caa0b` FOREIGN KEY (`college_id`) REFERENCES `onlinetest_college` (`id`),
  CONSTRAINT `user_id_refs_id_b15ccba5` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onlinetest_student`
--

LOCK TABLES `onlinetest_student` WRITE;
/*!40000 ALTER TABLE `onlinetest_student` DISABLE KEYS */;
/*!40000 ALTER TABLE `onlinetest_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionbank_categorizedquestion`
--

DROP TABLE IF EXISTS `questionbank_categorizedquestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionbank_categorizedquestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` longtext NOT NULL,
  `correct_id` int(11) NOT NULL,
  `option1_id` int(11) NOT NULL,
  `option2_id` int(11) NOT NULL,
  `option3_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `frequency` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questionbank_categorizedquestion_6ac947e3` (`correct_id`),
  KEY `questionbank_categorizedquestion_8fe35c51` (`option1_id`),
  KEY `questionbank_categorizedquestion_bad75838` (`option2_id`),
  KEY `questionbank_categorizedquestion_2058be1d` (`option3_id`),
  KEY `questionbank_categorizedquestion_42dc49bc` (`category_id`),
  KEY `questionbank_categorizedquestion_913b3835` (`subcategory_id`),
  CONSTRAINT `option3_id_refs_id_66b5e416` FOREIGN KEY (`option3_id`) REFERENCES `questionbank_option` (`id`),
  CONSTRAINT `category_id_refs_id_4e3466ef` FOREIGN KEY (`category_id`) REFERENCES `questionbank_category` (`id`),
  CONSTRAINT `correct_id_refs_id_66b5e416` FOREIGN KEY (`correct_id`) REFERENCES `questionbank_option` (`id`),
  CONSTRAINT `option1_id_refs_id_66b5e416` FOREIGN KEY (`option1_id`) REFERENCES `questionbank_option` (`id`),
  CONSTRAINT `option2_id_refs_id_66b5e416` FOREIGN KEY (`option2_id`) REFERENCES `questionbank_option` (`id`),
  CONSTRAINT `subcategory_id_refs_id_ca223392` FOREIGN KEY (`subcategory_id`) REFERENCES `questionbank_subcategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionbank_categorizedquestion`
--

LOCK TABLES `questionbank_categorizedquestion` WRITE;
/*!40000 ALTER TABLE `questionbank_categorizedquestion` DISABLE KEYS */;
INSERT INTO `questionbank_categorizedquestion` VALUES (1,'<p>\r\n A can do a work in 15 days and B in 20 days. If they work on it together for 4 days, then the fraction of the work that is left is</p>',1,2,3,4,1,0,1),(2,'<p>\r\n  A, B and C can do a piece of work in 20, 30 and 60 days respectively. In how many days can A do the work if he is assisted by B and C on every third day</p>',5,6,7,8,1,0,1),(3,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">A alone can do a piece of work in 6 days and B alone in 8 days. A and B undertook to do it for Rs. 3200. With the help of C, they completed the work in 3 days. How much is to be paid to C</font></span></p>',9,10,11,12,1,0,1),(4,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">If 6 men and 8 boys can do a piece of work in 10 days while 26 men and 48 boys can do the same in 2 days, the time taken by 15 men and 20 boys in doing the same type of work will be</font></span></p>',13,14,15,16,1,0,1),(5,'<p>\r\n  A machine P can print one lakh books in 8 hours, machine Q can print the same number of books in 10 hours while machine R can print them in 12 hours. All the machines are started at 9 A.M. while machine P is closed at 11 A.M. and the remaining two machines complete work. Approximately at what time will the work (to print one lakh books) be finished?</p>',17,18,19,20,1,0,1),(6,'<p>\r\n 4 men and 6 women can complete a work in 8 days, while 3 men and 7 women can complete it in 10 days. In how many days will 10 women complete it?</p>',21,22,23,24,1,0,1),(7,'<p>\r\n  A and B can together finish a work 30 days. They worked together for 20 days and then B left. After another 20 days, A finished the remaining work. In how many days A alone can finish the work?</p>',25,26,21,27,1,0,1),(8,'<p>\r\n X and Y can do a piece of work in 20 days and 12 days respectively. X started the work alone and then after 4 days Y joined him till the completion of the work. How long did the work last?</p>',28,15,5,29,1,0,1),(9,'<p>\r\n From a group of 7 men and 6 women, five persons are to be selected to form a committee so that at least 3 men are there on the committee. In how many ways can it be done?</p>',30,31,32,33,1,0,2),(10,'<p>\r\n In how many different ways can the letters of the word &#39;CORPORATION&#39; be arranged so that the vowels always come together?</p>',34,35,36,37,1,0,2),(11,'<p>\r\n  In a group of 6 boys and 4 girls, four children are to be selected. In how many different ways can they be selected such that at least one boy should be there?</p>',38,39,40,41,1,0,2),(12,'<p>\r\n  A box contains 2 white balls, 3 black balls and 4 red balls. In how many ways can 3 balls be drawn from the box, if at least one black ball is to be included in the draw?</p>',42,43,44,45,1,0,2),(13,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">In how many ways can a group of 5 men and 2 women be made out of a total of 7 men and 3 women</font></span> ?</p>',46,47,48,49,1,0,2),(14,'<p>\r\n In how many different ways can the letters of the word &#39;OPTICAL&#39; be arranged so that the vowels always come together?</p>',50,51,52,53,1,0,2),(15,'<p>\r\n  How many 3-digit numbers can be formed from the digits 2, 3, 5, 6, 7 and 9, which are divisible by 5 and none of the digits is repeated?</p>',54,55,56,57,1,0,2),(16,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">In how many different ways can the letters of the word &#39;DETAIL&#39; be arranged in such a way that the vowels occupy only the odd positions?</font></span><br />\r\n &nbsp;</p>',58,59,60,61,1,0,2),(17,'<p>\r\n Father is aged three times more than his son Ronit. After 8 years, he would be two and a half times of Ronit&#39;s age. After further 8 years, how many times would he be of Ronit&#39;s age?</p>',62,63,64,65,1,0,3),(19,'<p>\r\n  A father said to his son, &quot;I was as old as you are at the present at the time of your birth&quot;. If the father&#39;s age is 38 years now, the son&#39;s age five years back was</p>',67,68,69,70,1,0,3),(20,'<p>\r\n A is two years older than B who is twice as old as C. If the total of the ages of A, B and C be 27, the how old is B?</p>',55,71,72,73,1,0,3),(21,'<p>\r\n  Present ages of Sameer and Anand are in the ratio of 5 : 4 respectively. Three years hence, the ratio of their ages will become 11 : 9 respectively. What is Anand&#39;s present age in years?</p>',74,75,76,77,1,0,3),(22,'<p>\r\n Six years ago, the ratio of the ages of Kunal and Sagar was 6 : 5. Four years hence, the ratio of their ages will be 11 : 10. What is Sagar&#39;s age at present?</p>',78,79,80,81,1,0,3),(23,'<p>\r\n  At present, the ratio between the ages of Arun and Deepak is 4 : 3. After 6 years, Arun&#39;s age will be 26 years. What is the age of Deepak at present ?</p>',82,83,84,85,1,0,3),(24,'<p>\r\n Sachin is younger than Rahul by 7 years. If their ages are in the respective ratio of 7 : 9, how old is Sachin?</p>',86,87,88,89,1,0,3),(25,'<p>\r\n The present ages of three persons in proportions 4 : 7 : 9. Eight years ago, the sum of their ages was 56. Find their present ages (in years).</p>',90,91,92,77,1,0,3),(26,'<p>\r\n What least number must be added to 1056, so that the sum is completely divisible by 23 ?</p>',93,94,79,77,1,0,4),(27,'<p>\r\n How many of the following numbers are divisible by 132 ?<br />\r\n  264, 396, 462, 792, 968, 2178, 5184, 6336</p>',95,57,96,97,1,0,4),(28,'<p>\r\n  The largest 4 digit number exactly divisible by 88 is</p>',98,99,100,101,1,0,4),(29,'<p>\r\n  A number when divided by 296 leaves 75 as remainder. When the same number is divided by 37, the remainder will be</p>',102,93,73,71,1,0,4),(30,'<p>\r\n What smallest number should be added to 4456 so that the sum is completely divisible by 6 ?</p>',95,94,93,102,1,0,4),(31,'<p>\r\n When a number is divided by 13, the remainder is 11. When the same number is divided by 17, then remainder is 9. What is the number ?</p>',103,104,105,77,1,0,4),(32,'<p>\r\n The difference of the squares of two consecutive even integers is divisible by which of the following integers ?</p>',95,94,57,96,1,0,4),(33,'<p>\r\n The sum all even natural numbers between 1 and 31 is:</p>',106,78,107,108,1,0,4),(34,'<p>\r\n Today is Monday. After 61 days, it will be:</p>',109,110,111,112,1,0,5),(35,'<p>\r\n  <font face=\"Verdana\" size=\"2\">It was Monday on Jan 1, 2007. What was the day of the week Jan 1, 2011?</font></p>',113,114,115,116,1,0,5),(36,'<p>\r\n What was the day of the week on 17<sup><font size=\"2\">th</font></sup> June, 1998?</p>',111,117,112,110,1,0,5),(37,'<p>\r\n  On what dates of April, 2001 did Wednesday fall?</p>',118,119,120,121,1,0,5),(38,'<p>\r\n Find the angle between the minute hand and hour hand of a click when the time is 7.20?</p>',122,123,124,125,1,0,5),(39,'<p>\r\n A clock is set right at 8 a.m. The clock gains 10 minutes in 24 hours. What will be the true time when the clock indicates 1 p.m. on the following day?</p>',126,127,128,77,1,0,5),(41,'<p>\r\n  The angle between the minute hand and the hour hand of a clock when the time is 4.20, is</p>',129,130,131,130,1,0,5),(42,'<p>\r\n How many times in a day, are the hands of a clock in straight line but opposite in direction?</p>',132,80,74,60,1,0,5),(43,'<p>\r\n A can complete a project in 20 days and B can complete the same project in 30 days. If A and B start working on the project together and A quits 10 days before the project is completed, in how many days will the project be completed?</p>',133,134,135,136,1,0,1),(44,'<p>\r\n Ram, who is half as efficient as Krish, will take 24 days to complete a work if he worked alone. If Ram and Krish worked together, how long will they take to complete the work?</p>',137,135,138,136,1,0,1),(45,'<p>\r\n Ten coins are tossed simultaneously. In how many of the outcomes will the third coin turn up a head?</p>',139,140,141,142,1,0,2),(46,'<p>\r\n What is the probability that the position in which the consonants appear remain unchanged when the letters of the word Math are re-arranged?</p>',143,144,145,146,1,0,2),(47,'<p>\r\n Jan is twice as old as jack. The sum of there ages is 5 times Jakes age less 48.How old are jan and Jack ?</p>',147,148,149,77,1,0,3),(48,'<p>\r\n  Pete, Bryan and Philip are cousins. Pete&rsquo;s age is one-third of Bryan and Philip is five years elder than Bryan. If the sum of the age of the cousins is 40, find the ages of each<br />\r\n &nbsp;</p>',150,151,152,77,1,0,3),(49,'<p>\r\n  How many keystrokes are needed to type numbers from 1 to 1000?</p>',153,154,155,77,1,0,4),(50,'<p>\r\n  Which one of the following can&#39;t be the square of natural number ?</p>',156,157,158,77,1,0,4),(51,'<p>\r\n  A clock is started at noon. By 10 minutes past 5, the hour hand has turned through:</p>',159,160,161,162,1,0,5),(52,'<p>\r\n  How many times are the hands of a clock at right angle in a day?</p>',163,132,74,60,1,0,5);
/*!40000 ALTER TABLE `questionbank_categorizedquestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionbank_category`
--

DROP TABLE IF EXISTS `questionbank_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionbank_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionbank_category`
--

LOCK TABLES `questionbank_category` WRITE;
/*!40000 ALTER TABLE `questionbank_category` DISABLE KEYS */;
INSERT INTO `questionbank_category` VALUES (1,'MATH'),(2,'LOGICAL'),(3,'RC'),(4,'DI');
/*!40000 ALTER TABLE `questionbank_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionbank_multiquestion`
--

DROP TABLE IF EXISTS `questionbank_multiquestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionbank_multiquestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` longtext NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `frequency` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `questionbank_multiquestion_42dc49bc` (`category_id`),
  KEY `questionbank_multiquestion_913b3835` (`subcategory_id`),
  CONSTRAINT `category_id_refs_id_ab7fe8a1` FOREIGN KEY (`category_id`) REFERENCES `questionbank_category` (`id`),
  CONSTRAINT `subcategory_id_refs_id_e05f67e` FOREIGN KEY (`subcategory_id`) REFERENCES `questionbank_subcategory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionbank_multiquestion`
--

LOCK TABLES `questionbank_multiquestion` WRITE;
/*!40000 ALTER TABLE `questionbank_multiquestion` DISABLE KEYS */;
/*!40000 ALTER TABLE `questionbank_multiquestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionbank_option`
--

DROP TABLE IF EXISTS `questionbank_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionbank_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionbank_option`
--

LOCK TABLES `questionbank_option` WRITE;
/*!40000 ALTER TABLE `questionbank_option` DISABLE KEYS */;
INSERT INTO `questionbank_option` VALUES (1,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">8 /15</font></span></p>'),(2,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">1 /4</font></span></p>'),(3,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">1 /10</font></span></p>'),(4,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">7 /15</font></span></p>'),(5,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">15 days</font></span></p>'),(6,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">12 days</font></span></p>'),(7,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">16 days</font></span></p>'),(8,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">18 days</font></span></p>'),(9,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">Rs. 400</font></span></p>'),(10,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">Rs. 375</font></span></p>'),(11,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">Rs. 600</font></span></p>'),(12,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">Rs. 800</font></span></p>'),(13,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">4 days</font></span></p>'),(14,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">5 days</font></span></p>'),(15,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">6 days</font></span></p>'),(16,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">7 days</font></span></p>'),(17,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">1:00 P.M.</font></span></p>'),(18,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">12 Noon</font></span></p>'),(19,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">11:30 A.M.</font></span></p>'),(20,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">12:30 P.M</font></span></p>'),(21,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">40</font></span></p>'),(22,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">35</font></span></p>'),(23,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">45</font></span></p>'),(24,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">50</font></span></p>'),(25,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">60</font></span></p>'),(26,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">50 </font></span></p>'),(27,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">54</font></span></p>'),(28,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">10 days </font></span></p>'),(29,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Times New Roman&quot;,&quot;serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">20 days</font></span></p>'),(30,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">756 </font></span></p>'),(31,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">564</font></span></p>'),(32,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">645 </font></span></p>'),(33,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">735</font></span></p>'),(34,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">5760</font></span></p>'),(35,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">50400 </font></span></p>'),(36,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">2880</font></span></p>'),(37,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">1440 </font></span></p>'),(38,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">209 </font></span></p>'),(39,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">194 </font></span></p>'),(40,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">159</font></span></p>'),(41,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">None of these</font></span></p>'),(42,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">64 </font></span></p>'),(43,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">96 </font></span></p>'),(44,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">32 </font></span></p>'),(45,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">48 </font></span></p>'),(46,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">63 </font></span></p>'),(47,'<p>\r\n 90</p>'),(48,'<p>\r\n 126</p>'),(49,'<p>\r\n  45</p>'),(50,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">720 </font></span></p>'),(51,'<p>\r\n  120</p>'),(52,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">4320</font></span></p>'),(53,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">2160 </font></span></p>'),(54,'<p>\r\n <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">20 </font></span></p>'),(55,'<p>\r\n 10</p>'),(56,'<p>\r\n 15</p>'),(57,'<p>\r\n 5</p>'),(58,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">36</font></span></p>'),(59,'<p>\r\n  32</p>'),(60,'<p>\r\n 48</p>'),(61,'<p>\r\n 60</p>'),(62,'<p>\r\n 2 Times</p>'),(63,'<p>\r\n  2.5 Times</p>\r\n<p>\r\n  &nbsp;</p>'),(64,'<p>\r\n 3 Times</p>'),(65,'<p>\r\n  3.5 Times</p>'),(66,'<p>\r\n  <span style=\"line-height: 115%; font-family: &quot;Calibri&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: Calibri; mso-ascii-theme-font: minor-latin; mso-fareast-theme-font: minor-latin; mso-hansi-theme-font: minor-latin; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-bidi-theme-font: minor-bidi; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;\"><font color=\"#000000\">120</font></span></p>'),(67,'<p>\r\n 14 years</p>'),(68,'<p>\r\n 19 years</p>'),(69,'<p>\r\n 33 years</p>'),(70,'<p>\r\n 38 years</p>'),(71,'<p>\r\n 11</p>'),(72,'<p>\r\n 9</p>'),(73,'<p>\r\n  8</p>'),(74,'<p>\r\n  24</p>'),(75,'<p>\r\n 27</p>'),(76,'<p>\r\n 40</p>'),(77,'<p>\r\n None of these</p>'),(78,'<p>\r\n  16</p>'),(79,'<p>\r\n 18</p>'),(80,'<p>\r\n 20</p>'),(81,'<p>\r\n Cannot be determined</p>'),(82,'<p>\r\n 15 Years</p>'),(83,'<p>\r\n 12 Years</p>'),(84,'<p>\r\n 19 And Half Year</p>'),(85,'<p>\r\n 21 Year</p>'),(86,'<p>\r\n  24.5 years</p>'),(87,'<p>\r\n 18 years</p>'),(88,'<p>\r\n 28 years</p>'),(89,'<p>\r\n 16 years</p>'),(90,'<p>\r\n 16, 28, 36</p>'),(91,'<p>\r\n 8, 20, 28</p>'),(92,'<p>\r\n  20, 35, 45</p>'),(93,'<p>\r\n 2</p>'),(94,'<p>\r\n  3</p>'),(95,'<p>\r\n  4</p>'),(96,'<p>\r\n  6</p>'),(97,'<p>\r\n  7</p>'),(98,'<p>\r\n  9944</p>'),(99,'<p>\r\n 9768</p>'),(100,'<p>\r\n  9988</p>'),(101,'<p>\r\n  8888</p>'),(102,'<p>\r\n  1</p>'),(103,'<p>\r\n 349</p>'),(104,'<p>\r\n 339</p>'),(105,'<p>\r\n 359</p>'),(106,'<p>\r\n 240</p>'),(107,'<p>\r\n 128</p>'),(108,'<p>\r\n 512</p>'),(109,'<p>\r\n Saturday</p>'),(110,'<p>\r\n  Thursday</p>'),(111,'<p>\r\n  Wednesday</p>'),(112,'<p>\r\n Tuesday</p>'),(113,'<p>\r\n <font face=\"Verdana\" size=\"2\">Saturday</font></p>'),(114,'<p>\r\n <font face=\"Verdana\" size=\"2\">Monday</font></p>'),(115,'<p>\r\n <font face=\"Verdana\" size=\"2\">Wednesday</font></p>'),(116,'<p>\r\n  <font face=\"Verdana\" size=\"2\">Friday</font></p>'),(117,'<p>\r\n Monday</p>'),(118,'<p>\r\n  4<sup><font size=\"2\">th</font></sup>, 11<sup><font size=\"2\">th</font></sup>, 18<sup><font size=\"2\">th</font></sup>, 25<sup><font size=\"2\">th</font></sup></p>'),(119,'<p>\r\n 2<sup><font size=\"2\">nd</font></sup>, 9<sup><font size=\"2\">th</font></sup>, 16<sup><font size=\"2\">th</font></sup>, 23<sup><font size=\"2\">rd</font></sup>, 30<sup><font size=\"2\">th</font></sup></p>'),(120,'<p>\r\n 3<sup><font size=\"2\">rd</font></sup>, 10<sup><font size=\"2\">th</font></sup>, 17<sup><font size=\"2\">th</font></sup>, 24<sup><font size=\"2\">th</font></sup></p>'),(121,'<p>\r\n 1<sup><font size=\"2\">st</font></sup>, 8<sup><font size=\"2\">th</font></sup>, 15<sup><font size=\"2\">th</font></sup>, 22<sup><font size=\"2\">nd</font></sup>, 29<sup><font size=\"2\">th</font></sup></p>'),(122,'<p>\r\n 100 deg</p>'),(123,'<p>\r\n 110 deg</p>'),(124,'<p>\r\n 98 deg</p>'),(125,'<p>\r\n  120 deg</p>'),(126,'<p>\r\n 48 min. past 12.</p>'),(127,'<p>\r\n  45 min past 21</p>'),(128,'<p>\r\n  49 min past 31</p>'),(129,'<p>\r\n  10&ordm;</p>'),(130,'<p>\r\n  20&ordm;</p>'),(131,'<p>\r\n  0&ordm;</p>'),(132,'<p>\r\n 22</p>'),(133,'<p>\r\n  18 day</p>'),(134,'<p>\r\n  27 days</p>'),(135,'<p>\r\n 12 days</p>'),(136,'<p>\r\n 16 days</p>'),(137,'<p>\r\n 8 days</p>'),(138,'<p>\r\n  6 days</p>'),(139,'<p>\r\n  2<sup><font size=\"2\">9</font></sup></p>'),(140,'<p>\r\n 2<sup><font size=\"2\">10</font></sup></p>'),(141,'<p>\r\n  3 * 2<sup><font size=\"2\">8</font></sup></p>'),(142,'<p>\r\n 3 * 2<sup><font size=\"2\">9</font></sup></p>'),(143,'<p>\r\n 1/4</p>'),(144,'<p>\r\n 1/6</p>'),(145,'<p>\r\n 1/3<br />\r\n &nbsp;</p>'),(146,'<p>\r\n  1/24</p>'),(147,'<p>\r\n  24,48</p>'),(148,'<p>\r\n 12,24</p>'),(149,'<p>\r\n 13,26</p>'),(150,'<p>\r\n 5,15,20</p>'),(151,'<p>\r\n 7,19,21</p>'),(152,'<p>\r\n 6,16,18</p>'),(153,'<p>\r\n 2893</p>'),(154,'<p>\r\n  3001</p>'),(155,'<p>\r\n  2704</p>'),(156,'<p>\r\n  42437</p>'),(157,'<p>\r\n 20164</p>'),(158,'<p>\r\n 32761</p>'),(159,'<p>\r\n 155&ordm;</p>'),(160,'<p>\r\n 160&ordm;</p>'),(161,'<p>\r\n 150&ordm;</p>'),(162,'<p>\r\n 145&ordm;</p>'),(163,'<p>\r\n 44</p>');
/*!40000 ALTER TABLE `questionbank_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionbank_subcategory`
--

DROP TABLE IF EXISTS `questionbank_subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionbank_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionbank_subcategory`
--

LOCK TABLES `questionbank_subcategory` WRITE;
/*!40000 ALTER TABLE `questionbank_subcategory` DISABLE KEYS */;
INSERT INTO `questionbank_subcategory` VALUES (1,'Time&Work'),(2,'Per&Comb'),(3,'AgeProblem'),(4,'Numbers'),(5,'Calendar');
/*!40000 ALTER TABLE `questionbank_subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionbank_subquestion`
--

DROP TABLE IF EXISTS `questionbank_subquestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionbank_subquestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` longtext NOT NULL,
  `correct_id` int(11) NOT NULL,
  `option1_id` int(11) NOT NULL,
  `option2_id` int(11) NOT NULL,
  `option3_id` int(11) NOT NULL,
  `para_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `questionbank_subquestion_6ac947e3` (`correct_id`),
  KEY `questionbank_subquestion_8fe35c51` (`option1_id`),
  KEY `questionbank_subquestion_bad75838` (`option2_id`),
  KEY `questionbank_subquestion_2058be1d` (`option3_id`),
  KEY `questionbank_subquestion_455ba00e` (`para_id`),
  CONSTRAINT `option3_id_refs_id_9b635e6d` FOREIGN KEY (`option3_id`) REFERENCES `questionbank_option` (`id`),
  CONSTRAINT `correct_id_refs_id_9b635e6d` FOREIGN KEY (`correct_id`) REFERENCES `questionbank_option` (`id`),
  CONSTRAINT `option1_id_refs_id_9b635e6d` FOREIGN KEY (`option1_id`) REFERENCES `questionbank_option` (`id`),
  CONSTRAINT `option2_id_refs_id_9b635e6d` FOREIGN KEY (`option2_id`) REFERENCES `questionbank_option` (`id`),
  CONSTRAINT `para_id_refs_id_45179cf8` FOREIGN KEY (`para_id`) REFERENCES `questionbank_multiquestion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionbank_subquestion`
--

LOCK TABLES `questionbank_subquestion` WRITE;
/*!40000 ALTER TABLE `questionbank_subquestion` DISABLE KEYS */;
/*!40000 ALTER TABLE `questionbank_subquestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reports_report`
--

DROP TABLE IF EXISTS `reports_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `marks_obtained` double NOT NULL,
  `lr_marks` double NOT NULL,
  `mathematics_marks` double NOT NULL,
  `rc_marks` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reports_report_55f4c957` (`sid_id`),
  CONSTRAINT `sid_id_refs_id_1ab4359e` FOREIGN KEY (`sid_id`) REFERENCES `onlinetest_student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reports_report`
--

LOCK TABLES `reports_report` WRITE;
/*!40000 ALTER TABLE `reports_report` DISABLE KEYS */;
/*!40000 ALTER TABLE `reports_report` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-09-05 17:38:58
