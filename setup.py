#!/usr/bin/env python
# -*- coding:utf8 -*-

import os
from setuptools import setup, find_packages

ver = '0.1'
README = os.path.join(os.path.dirname(__file__), 'README')
long_desc = open(README).read() + '\n\n'

setup(name='ExOnlineTest',
      version=ver,
      author='Arun K.R.',
      author_email='the1.arun@gmail.com',
      url='http://www.expicient.com',
      license='Simplified BSD',
      description='Online Objective Test tool + Question Bank manager.',
      long_description=long_desc,
      keywords='onlinetest',
      requires=['Django', ],
      install_requires=['setuptools',],
      packages=find_packages(),
)
