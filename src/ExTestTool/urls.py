from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),

    url(r'^$', 'onlinetest.views.register'), # register a user
    url(r'^pbyc/(?P<cid>(\d+))/$', 'onlinetest.views.papers_by_college'), #question papers by college
    url(r'^test/(?P<qid>(\d+))/$', 'onlinetest.views.test', name="exam"), # question id
    url(r'^mark/(?P<qid>(\d+))/$', 'onlinetest.views.book_mark', {'add': True}, name='mark'), # bookmark an item
    url(r'^unmark/(?P<qid>(\d+))/$', 'onlinetest.views.book_mark', {'add': False}, name='unmark'), # remove bookmark 
    url(r'^save/$', 'onlinetest.views.store_answer', name='store'), # store answer in session
    url(r'^submit/$', 'onlinetest.views.submit'), # submit paper (saving answers from session)

    url(r'^reports/', 'reports.views.reports'),
    url(r'^save_report/', 'reports.views.save_report'),
    url(r'^report_mode/', 'reports.views.report_mode'),
    url(r'^view_coll_report/', 'reports.views.view_coll_report'),
    url(r'^get_batches/(?P<cid>[\w\d\s]+)/$', 'reports.views.get_batches', name="get_batches")
)
