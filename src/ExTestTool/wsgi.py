import os
import sys
import site

# One directory above the project, so project name will be needed for imports
root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

# with mod_wsgi >= 2.4, this line will add this path in front of the python path
site.addsitedir(root_dir)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ExTestTool.settings")

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
