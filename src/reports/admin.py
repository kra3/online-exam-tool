from django.contrib import admin
from reports.models import Report

#class ReportAdmin(admin.ModelAdmin):
#      fields    = ['name', 'total_ques', 'total_answered', 'total_unanswered','di_answered','di_unanswered','lr_answered','lr_unanswered','mathematics_answered','mathematics_unanswered','rc_answered','rc_unanswered']

admin.site.register(Report)
