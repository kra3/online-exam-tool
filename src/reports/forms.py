from django import forms


ReportChoice = {'viewreport':'View Report'
                }


class InputForm(forms.Form):
#    filename = forms.CharField(max_length=256, label='File Name',required=False)
    mode = forms.ChoiceField(choices=ReportChoice.items(),required=False)