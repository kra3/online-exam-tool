from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from onlinetest.models import Student, QuestionPaper

class Report(models.Model):
    sid = models.ForeignKey(Student)
    name = models.CharField(max_length=250)
    marks_obtained = models.FloatField()
    lr_marks = models.FloatField()
    mathematics_marks = models.FloatField()
    rc_marks = models.FloatField()
    
    def __unicode__(self):
        return self.name
    

    
