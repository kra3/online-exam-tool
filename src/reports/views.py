import csv
from django.http import HttpResponse
from reports.models import Report
from django.shortcuts import get_object_or_404, render_to_response
from forms import InputForm

from onlinetest.models import College, QuestionPaper, Student


def reports(request):
    form = InputForm
    return render_to_response('report.html',{'form':form})
    
    
def save_report(request):
    # Create the HttpResponse object with the appropriate CSV header.
    file = request.GET['filename']
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s.csv'%file

    writer = csv.writer(response)
    data = Report.objects.all()
    writer.writerow(['name','marks_obtained','lr_marks','mathematics_marks','rc_marks'])
    for val in data:
        writer.writerow([val.name,val.marks_obtained,val.lr_marks,val.mathematics_marks,val.rc_marks])
#    writer.writerow(['First row', 'Foo', 'Bar', 'Baz'])
#    writer.writerow(['Second row', 'A', 'B', 'C', '"Testing"', "Here's a quote"])
    return response


def view_coll_report(request):
    data = College.objects.all()
    return render_to_response('view_coll_report.html',{'data':data})

    
def report_mode(request):
    if 'mode' in request.GET:
        mode = request.GET['mode']
        if mode == 'savereport':
            file = request.GET['filename']
            return save_report(request,file)
        if mode == 'viewreport':
            return view_coll_report(request)
        
        
def get_batches(request, cid):
    college = College.objects.filter(id=cid)
    cname = college[0].name
    address = college[0].street
    city = college[0].city
    batches = QuestionPaper.objects.filter(college=cid)
    stdinbatch = []
    candidates = []
    batchname=[]
    numbatches = xrange(0,len(batches))
    for batch in batches:
        batchname.append(str(batch))
        cands = Student.objects.filter(question_paper=batch)
        print len(cands)
        cand = []
        for val in cands:
            print val
            obj = Report.objects.filter(sid=val)
            if len(obj) != 0:
                obj[0].batch = str(batch)
            cand.extend(obj)
        candidates.extend(cand)
        stdinbatch.append(len(cand))
    return render_to_response('get_batches.html', {'data':batches,
        'collname':cname,
        'candidates_list':candidates,
        'address':address,
        'city':city,
        'batchlen':stdinbatch,
        'numbatches':numbatches,
        'batchnamelist':batchname})


def next(value, arg):
    try:
        return value[int(arg)-1]
    except:
        return None


def get_candidates(request, bid):
    cands = Student.objects.filter(bid=bid)
    cand = []
    for val in cands:
        cand.extend(Report.objects.filter(sid=val))
    
    return render_to_response('get_candidates.html',{'data':cand})
    
    