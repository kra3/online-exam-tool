# -*- coding: utf-8 -*-

import random

from django.db.models.signals import post_save
from django.dispatch import receiver

from onlinetest.models import QuestionPaper
from onlinetest.models import QuestionItem
from questionbank.models import DIQuestion
from questionbank.models import RCQuestion
from questionbank.models import MathQuestion
from questionbank.models import LogicalQuestion
from questionbank.models import SubQuestion


@receiver(post_save, sender=QuestionPaper, dispatch_uid="add_questions_to_questionpaper")
def create_question_paper(sender, **kwargs):
    obj = kwargs['instance']
    created = kwargs['created']

    if created: # add questions worth 30 marks into question paper, when it's first created.
        questions = []

        #Fetching 5 Math questions
        math_questions = MathQuestion.objects.order_by('frequency')[:10]
        if len(math_questions) >= 5:
            questions.extend(random.sample(math_questions, 5)) #get 10 less freaquenzed items and randomly pick 5

        #Fetching 10 Logical questions
        logical_questions = LogicalQuestion.objects.order_by('frequency')[:25]
        if len(logical_questions) >= 10:
            questions.extend(random.sample(logical_questions, 10)) #get 25 less freaquenzed items and randomly pick 10

        for question in questions:
            # add fetched questions into question paper
            qitem = QuestionItem(question_paper=obj, category=question.category, content_object=question, answer=question.correct)
            qitem.save()

            # and increase frequency by one. The higher the frequesncy, the higher it had been used in question papers.
            question.frequency += 1
            question.save()

        questions = []
        
        #Fetching 1 DI question
        data_interpretation_questions = DIQuestion.objects.order_by('frequency')[:5]
        if len(data_interpretation_questions) >= 1:
            questions.extend(random.sample(data_interpretation_questions, 1))  #get 5 less freaquenzed items and randomly pick 1

        #Fetching 2 RI question
        reading_comprehension_questoins = RCQuestion.objects.order_by('frequency')[:5]
        if len(reading_comprehension_questoins) >= 2:
            questions.extend(random.sample(reading_comprehension_questoins, 2)) #get 5 less freaquenzed items and randomly pick 2

        for question in questions:
            sq = SubQuestion.objects.filter(para = question)
            for s in sq:
                # add fetched questions into question paper
                qitem = QuestionItem(question_paper=obj, category=question.category, content_object=question, question_item=s, answer=s.correct)
                qitem.save()

            # and increase frequency by one. The higher the frequesncy, the higher it had been used in question papers.
            question.frequency += 1
            question.save()