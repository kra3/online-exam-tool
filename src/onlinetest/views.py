# from django.contrib.contenttypes.models import ContentType
# from django.db.models import Q, Count, Min
# from django.shortcuts import get_object_or_404, render_to_response
# from django.template import RequestContext
# from django.contrib.admin.views.decorators import staff_member_required

# from questionbank.models import DIQuestion, RCQuestion, MathQuestion, LogicalQuestion, Question, MultiQuestion, SubQuestion, Category, SubCategory
# from onlinetest.models import College, QuestionPaper, Student, AnswerItem
# from questionbank.models import CategorizedQuestion, Option

from collections import defaultdict
import datetime
import json
import random

from django import forms
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.decorators.http import require_POST

from onlinetest.models import AnswerItem
from onlinetest.models import QuestionItem
from onlinetest.models import QuestionPaper
from onlinetest.models import Student
from questionbank.models import CategorizedQuestion
from questionbank.models import MultiQuestion
from questionbank.models import Option
from questionbank.models import SubQuestion
from reports.models import Report

"""
IN_PH = re.compile(
    (?P<std_code>                   # the std-code group
        ^0                          # all std-codes start with 0
        (
            (?P<twodigit>\d{2})   | # either two, three or four digits
            (?P<threedigit>\d{3}) | # following the 0
            (?P<fourdigit>\d{4})
        )
    )
    [-\s]                           # space or -
    (?P<phone_no>                   # the phone number group
        [1-6]                       # first digit of phone number
        (
            (?(twodigit)\d{7})   |  # 7 more phone digits for 3 digit stdcode
            (?(threedigit)\d{6}) |  # 6 more phone digits for 4 digit stdcode
            (?(fourdigit)\d{5})     # 5 more phone digits for 5 digit stdcode
        )
    )
)$, re.VERBOSE)

"""

def register(request):
    """
    Register a user. That's collect informations from him & start the test

    """
    class StudentForm(forms.ModelForm):
        class Meta:
            model = Student
            exclude = ('user', )
            widgets = {
                'college': forms.Select(attrs={'onchange':'FilterQuestionPapers(this);'}),
                'question_paper': forms.Select(attrs={'disabled':'false'}),
            }

    if request.method == 'POST': # If the form has been submitted...
        form = StudentForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # create a user 
            user = User.objects.create_user(username=form.data['email'], password=form.data['email'], email=form.data['email'])
            user.first_name = form.data['name']
            user.is_staff = False
            user.save()

            # add django user to our student relation
            obj = form.save(commit=False)            
            obj.user = user
            obj.save()

            #logs our temp user in
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)

            # get all question's ids categorywise randomized
            qitems = QuestionItem.objects.filter(question_paper = obj.question_paper).order_by('category', 'object_id', '?').only('id', 'category__name')
            
            # put question ids in session (order will be different for different users)
            request.session['qids'] = [q.id for q in qitems]

            # get starting items of each category
            cat_beg = defaultdict(list)
            for q in qitems:
                cat_beg[q.category.name].append(int(q.id))

            # set initial qids for each category in session
            request.session['cat_beg'] = cat_beg

            # set start & end time in session (of test)
            started_at = datetime.datetime.now()
            should_end_at = started_at + datetime.timedelta(minutes=int(obj.question_paper.duration)) 
            request.session['started_at'] = started_at
            request.session['should_end_at'] = should_end_at

            #redirect to test home page
            return HttpResponseRedirect('/test/%s/' % qitems[0].id) # Redirect after POST
    else:
        form = StudentForm() # An unbound form
    # display registration form and instructions
    return render(request, 'register.html', {'form': form,})

def papers_by_college(request, cid):
    question_papers = QuestionPaper.objects.filter(college__id=cid)
    data = {question_paper.pk: str(question_paper) for question_paper in question_papers if question_paper.deployed}
    return HttpResponse(json.dumps(data), mimetype="application/json")

@login_required(login_url='/') # add a page which says session expiered
@user_passes_test(lambda u: not u.is_staff)
def test(request, qid):
    '''
    qid: QuestionItem ID, which we will compare against the one in session 
    sqid: SubQuestion ID for MultiQuestion types (RC, DI), default [None]
    '''
    if not long(qid) in request.session.get('qids', []):
        return HttpResponse("Huh, Playing with me?")

    now = datetime.datetime.now()
    if not (now < request.session['should_end_at'] and now > request.session['started_at']):
       return HttpResponseRedirect('/submit/') # Exam time is over, let's submit it.

    # time management
    remaining_time = request.session['should_end_at'] - now
    remaining_minutes = "%02d" % (remaining_time.seconds / 60, )
    remaining_seconds = "%02d" % (remaining_time.seconds % 60, )

    # get QuestionItem
    qitem = QuestionItem.objects.get(pk=qid)
    question_obj = qitem.content_object
    data = None
    question = None
    options = []

    # now populate question, options and data(optional)
    if isinstance(question_obj, CategorizedQuestion):
        question = question_obj.question
        options.insert(0, question_obj.correct)
        options.insert(0, question_obj.option1)
        options.insert(0, question_obj.option2)
        options.insert(0, question_obj.option3)

    if isinstance(question_obj, MultiQuestion):
        data = question_obj.data
        sub_question = qitem.question_item
        question = sub_question.question
        options.insert(0, sub_question.correct)
        options.insert(0, sub_question.option1)
        options.insert(0, sub_question.option2)
        options.insert(0, sub_question.option3)
    
    # Position of correct option
    options_pos = request.session.get(qid, None)
    if not options_pos:
        random.shuffle(options) #shuffle options
        options_pos = {i: j for i,j in enumerate(options)}
        request.session[qid] = options_pos

    # next and previous questions
    qids = request.session['qids'] # by now we can assume qids is in session
    indx = qids.index(long(qid)) # also we can assume qid is in the qids list
    next = qids[indx+1] if indx<len(qids)-1 else None # len()-1 will be last index
    prev = qids[indx-1] if indx>0 else None # 0 is first inde

    # How many answerd
    student = Student.objects.get(user=request.user)
    answerd = AnswerItem.objects.filter(student=student).count()

    #if an option is previosuly answered we have to select it by default
    sel_opt = None
    try:
        qi = QuestionItem.objects.get(pk=qid)
        a = AnswerItem.objects.get(student=student, question_item=qi)
        sel_opt = a.answer_id
    except:
        pass

    bundle = {
        'cur': str(int(qid)),
        'next': next,
        'prev': prev,
        'remaining_minutes': remaining_minutes,
        'remaining_seconds': remaining_seconds,
        'para': data,
        'question': question,
        'options': options_pos,
        'cat_beg': dict(request.session['cat_beg']),
        'bookmarks': request.session.get('bookmarks', []),
        'answerd': answerd,
        'sel_opt': sel_opt,
    }
    return render(request, 'exam.html', bundle)

def book_mark(request, qid, add):
    bookmarks = request.session.get('bookmarks', [])
    qids = request.session['qids']
    if long(qid) in qids:
        if add:
            if qid not in bookmarks: bookmarks.insert(0, qid)
        else:
            if qid in bookmarks: bookmarks.remove(qid)

    request.session['bookmarks'] = bookmarks
    return HttpResponseRedirect('/test/%s' % qid) # redirect to same page

@require_POST
def store_answer(request):
    class AnswerItemForm(forms.ModelForm):
        class Meta:
            model = AnswerItem
            exclude = ('student', )

    form = AnswerItemForm(request.POST)
    if form.is_valid(): # All validation rules pass 
        student = Student.objects.get(user=request.user)
        try:
            question_item = QuestionItem.objects.get(pk=form.data['question_item'])
            answer = Option.objects.get(pk=form.data['answer'])
        except:
            data = {'msg': 'error'}
            return HttpResponse(json.dumps(data),mimetype="application/json") # Why this Kolavery da? we got malformed data

        obj = AnswerItem()
        try:
            obj = AnswerItem.objects.get(student=student, question_item=question_item)
        except AnswerItem.DoesNotExist:
            obj = AnswerItem()
            obj.student = student
            obj.question_item = question_item
        finally:
            obj.answer = answer
            obj.save()
        
        data = {'msg': 'success'}
        return HttpResponse(json.dumps(data),mimetype="application/json")

    data = {'msg': 'validationerror'}
    return HttpResponse(json.dumps(data),mimetype="application/json")

def submit(request):
    student = Student.objects.get(user=request.user)
    attempts = AnswerItem.objects.filter(student=student).order_by('question_item__category')

    marks = defaultdict(float)
    for item in attempts:
        if item.answer_id == item.question_item.answer_id:
            marks[item.question_item.category.name] += 1
        else:
            marks[item.question_item.category.name] -= 0.25

    #create the report for the student
    report = Report()
    report.sid = student
    report.name = str(student)
    report.lr_marks = marks.get('LOGICAL', 0)
    report.mathematics_marks = marks.get('MATH', 0) + marks.get('DI', 0)
    report.rc_marks = marks.get('RC', 0)    
    report.marks_obtained = report.lr_marks + report.mathematics_marks + report.rc_marks
    report.save()

    #log the user out and clear session
    request.session.clear()
    request.session.flush()
    logout(request)

    #redirect to regiser page
    return HttpResponseRedirect("/")
