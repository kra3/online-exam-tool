from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.contrib.sites.models import Site

from onlinetest.models import College
from onlinetest.models import Student
from onlinetest.models import QuestionPaper
from onlinetest.models import QuestionItem
from onlinetest import signals # This is needed to register signals to django


class StudentAdmin(admin.ModelAdmin):
    exclude = ('user', )
    def has_add_permission(self, request):
        return False


class QuestionItemInline(admin.TabularInline):
    model = QuestionItem
    extra = 0
    max_num = 18 # 5 Math + 1 DI + 10 LR + 2 RC 
    can_delete = False

    def has_delete_permission(self, request, obj=None):
        return False


class QuestionPaperAdmin(admin.ModelAdmin):
    #inlines = (QuestionItemInline,) #why do we have to show this? This will not aid users at all but to add to confusion!

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(QuestionPaperAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions


admin.site.register(College)
admin.site.register(Student, StudentAdmin)
admin.site.register(QuestionPaper, QuestionPaperAdmin)

# remove Auth & Site models from Admin Panel
admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.unregister(Site)