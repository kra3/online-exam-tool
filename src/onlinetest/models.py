# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models

from questionbank.models import Option, Category
from questionbank.models import DIQuestion
from questionbank.models import RCQuestion
from questionbank.models import MathQuestion
from questionbank.models import LogicalQuestion
from questionbank.models import SubQuestion


class College(models.Model):
    name = models.CharField(max_length=250)
    street = models.CharField(max_length=250)
    city = models.CharField(max_length=250)
    state = models.CharField(max_length=250)
    contact_no = models.CharField(max_length=12)

    def __unicode__(self):
        return self.name


class Student(models.Model):
    user = models.OneToOneField(User)
    college = models.ForeignKey('College')
    question_paper = models.ForeignKey('QuestionPaper')
    name = models.CharField(max_length=250)
    register_no = models.IntegerField()
    branch = models.CharField(max_length=250)
    email = models.EmailField(max_length=250)
    phone_no = models.CharField(max_length=12)

    def __unicode__(self):
        return self.name


class QuestionPaper(models.Model):
    college = models.ForeignKey('College')
    duration = models.PositiveSmallIntegerField(help_text='Duration of Exam (in minutes)', default=45)
    deployed = models.BooleanField(default=False)

    def __unicode__(self):
        return "Paper %d" % self.id

    def clean(self):
        if len(MathQuestion.objects.order_by('frequency')[:5]) < 5:
            raise ValidationError('Not enough Math Questions in Question Bank')
        if len(DIQuestion.objects.order_by('frequency')[:1])  < 1:            
            raise ValidationError('Not enough Data Interpretation Questions in Question Bank')
        if len(LogicalQuestion.objects.order_by('frequency')[:10]) < 10:
            raise ValidationError('Not enough Logical Questions in Question Bank')
        if len(RCQuestion.objects.order_by('frequency')[:2]) < 2:
            raise ValidationError('Not enough Reading Comprehension Questions in Question Bank')
            

class QuestionItem(models.Model):
    question_paper = models.ForeignKey('QuestionPaper')
    category = models.ForeignKey(Category) # to identify correct Question Type
    question_item = models.ForeignKey(SubQuestion, blank=True, null=True)    
    answer = models.ForeignKey(Option)
    object_id = models.PositiveIntegerField() # stores question here
    content_type = models.ForeignKey(ContentType, related_name="questions")
    content_object = generic.GenericForeignKey(ct_field='content_type', fk_field='object_id')

    def __unicode__(self):
        return "Question%d (Cat: %s, Paper: %d)" % (self.pk, self.category.name, self.question_paper_id)


class AnswerItem(models.Model):
    student = models.ForeignKey(Student)
    question_item = models.ForeignKey(QuestionItem)
    answer = models.ForeignKey(Option)
