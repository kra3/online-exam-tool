from ckeditor.widgets import CKEditor
from questionbank.models import Option

class ForeignCKEditor(CKEditor):
    def render(self, name, value, attrs=None, **kwargs):
        try:
            obj = Option.objects.get(pk=value)
        except:# ObjectDoesNotExist:
            pass
        else:
            value = obj.answer

        return super(ForeignCKEditor, self).render(name, value, attrs, **kwargs)
