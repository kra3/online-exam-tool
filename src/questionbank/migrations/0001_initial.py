# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table('questionbank_category', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal('questionbank', ['Category'])

        # Adding model 'SubCategory'
        db.create_table('questionbank_subcategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal('questionbank', ['SubCategory'])

        # Adding model 'Option'
        db.create_table('questionbank_option', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('answer', self.gf('ckeditor.fields.HTMLField')(max_length=250)),
        ))
        db.send_create_signal('questionbank', ['Option'])

        # Adding model 'CategorizedQuestion'
        db.create_table('questionbank_categorizedquestion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question', self.gf('ckeditor.fields.HTMLField')(max_length=250)),
            ('correct', self.gf('django.db.models.fields.related.ForeignKey')(related_name='questionbank_categorizedquestion_correct', to=orm['questionbank.Option'])),
            ('option1', self.gf('django.db.models.fields.related.ForeignKey')(related_name='questionbank_categorizedquestion_option1', to=orm['questionbank.Option'])),
            ('option2', self.gf('django.db.models.fields.related.ForeignKey')(related_name='questionbank_categorizedquestion_option2', to=orm['questionbank.Option'])),
            ('option3', self.gf('django.db.models.fields.related.ForeignKey')(related_name='questionbank_categorizedquestion_option3', to=orm['questionbank.Option'])),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['questionbank.Category'])),
            ('frequency', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('subcategory', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['questionbank.SubCategory'], null=True, blank=True)),
        ))
        db.send_create_signal('questionbank', ['CategorizedQuestion'])

        # Adding model 'MultiQuestion'
        db.create_table('questionbank_multiquestion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('data', self.gf('ckeditor.fields.HTMLField')()),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['questionbank.Category'])),
            ('subcategory', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['questionbank.SubCategory'], null=True, blank=True)),
            ('frequency', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('questionbank', ['MultiQuestion'])

        # Adding model 'SubQuestion'
        db.create_table('questionbank_subquestion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question', self.gf('ckeditor.fields.HTMLField')(max_length=250)),
            ('correct', self.gf('django.db.models.fields.related.ForeignKey')(related_name='questionbank_subquestion_correct', to=orm['questionbank.Option'])),
            ('option1', self.gf('django.db.models.fields.related.ForeignKey')(related_name='questionbank_subquestion_option1', to=orm['questionbank.Option'])),
            ('option2', self.gf('django.db.models.fields.related.ForeignKey')(related_name='questionbank_subquestion_option2', to=orm['questionbank.Option'])),
            ('option3', self.gf('django.db.models.fields.related.ForeignKey')(related_name='questionbank_subquestion_option3', to=orm['questionbank.Option'])),
            ('para', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['questionbank.MultiQuestion'])),
        ))
        db.send_create_signal('questionbank', ['SubQuestion'])


    def backwards(self, orm):
        # Deleting model 'Category'
        db.delete_table('questionbank_category')

        # Deleting model 'SubCategory'
        db.delete_table('questionbank_subcategory')

        # Deleting model 'Option'
        db.delete_table('questionbank_option')

        # Deleting model 'CategorizedQuestion'
        db.delete_table('questionbank_categorizedquestion')

        # Deleting model 'MultiQuestion'
        db.delete_table('questionbank_multiquestion')

        # Deleting model 'SubQuestion'
        db.delete_table('questionbank_subquestion')


    models = {
        'questionbank.categorizedquestion': {
            'Meta': {'object_name': 'CategorizedQuestion'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['questionbank.Category']"}),
            'correct': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'questionbank_categorizedquestion_correct'", 'to': "orm['questionbank.Option']"}),
            'frequency': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'option1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'questionbank_categorizedquestion_option1'", 'to': "orm['questionbank.Option']"}),
            'option2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'questionbank_categorizedquestion_option2'", 'to': "orm['questionbank.Option']"}),
            'option3': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'questionbank_categorizedquestion_option3'", 'to': "orm['questionbank.Option']"}),
            'question': ('ckeditor.fields.HTMLField', [], {'max_length': '250'}),
            'subcategory': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['questionbank.SubCategory']", 'null': 'True', 'blank': 'True'})
        },
        'questionbank.category': {
            'Meta': {'object_name': 'Category'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'questionbank.multiquestion': {
            'Meta': {'object_name': 'MultiQuestion'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['questionbank.Category']"}),
            'data': ('ckeditor.fields.HTMLField', [], {}),
            'frequency': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subcategory': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['questionbank.SubCategory']", 'null': 'True', 'blank': 'True'})
        },
        'questionbank.option': {
            'Meta': {'object_name': 'Option'},
            'answer': ('ckeditor.fields.HTMLField', [], {'max_length': '250'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'questionbank.subcategory': {
            'Meta': {'object_name': 'SubCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        'questionbank.subquestion': {
            'Meta': {'object_name': 'SubQuestion'},
            'correct': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'questionbank_subquestion_correct'", 'to': "orm['questionbank.Option']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'option1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'questionbank_subquestion_option1'", 'to': "orm['questionbank.Option']"}),
            'option2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'questionbank_subquestion_option2'", 'to': "orm['questionbank.Option']"}),
            'option3': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'questionbank_subquestion_option3'", 'to': "orm['questionbank.Option']"}),
            'para': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['questionbank.MultiQuestion']"}),
            'question': ('ckeditor.fields.HTMLField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['questionbank']