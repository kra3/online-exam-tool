# -*- coding: utf-8 -*-

from django.db import models
from ckeditor.fields import HTMLField


class Category(models.Model):
    name = models.CharField(max_length=10)

    def __unicode__(self):
        return self.name


class SubCategory(models.Model):
    name = models.CharField(max_length=10)

    def __unicode__(self):
        return self.name


class Option(models.Model):
    answer = HTMLField(max_length=250)

    def __unicode__(self):
        return self.answer


class Question(models.Model):
    question = HTMLField(max_length=250)
    correct = models.ForeignKey(Option, related_name='%(app_label)s_%(class)s_correct')
    option1 = models.ForeignKey(Option, related_name='%(app_label)s_%(class)s_option1')
    option2 = models.ForeignKey(Option, related_name='%(app_label)s_%(class)s_option2')
    option3 = models.ForeignKey(Option, related_name='%(app_label)s_%(class)s_option3')

    class Meta:
        abstract = True


class CategorizedQuestion(Question):
    category = models.ForeignKey(Category)
    frequency = models.IntegerField(default=0)
    subcategory = models.ForeignKey(SubCategory, blank=True, null=True)

    def __unicode__(self):
        return self.question


class MultiQuestion(models.Model):
    data = HTMLField()
    category = models.ForeignKey(Category)
    subcategory = models.ForeignKey(SubCategory, blank=True, null=True)
    frequency = models.IntegerField(default=0)

    def __unicode__(self):
        return self.data


class SubQuestion(Question):
    para = models.ForeignKey(MultiQuestion)

#### Model Managers: Since we are proxying same table for different category of question, ###
#### we have to filter questions based on category. ###

class DIManager(models.Manager):
    def get_query_set(self):
        category, created = Category.objects.get_or_create(name='DI')
        return super(DIManager, self).get_query_set().filter(category=category)


class RCManager(models.Manager):
    def get_query_set(self):
        category, created = Category.objects.get_or_create(name='RC')
        return super(RCManager, self).get_query_set().filter(category=category)


class MathManager(models.Manager):
    def get_query_set(self):
        category, created = Category.objects.get_or_create(name='MATH')
        return super(MathManager, self).get_query_set().filter(category=category)


class LogicalManager(models.Manager):
    def get_query_set(self):
        category, created = Category.objects.get_or_create(name='LOGICAL')
        return super(LogicalManager, self).get_query_set().filter(category=category)


##### Proxied Question Types: These are the available Categories of Questions ####

class DIQuestion(MultiQuestion):
    class Meta:
        proxy = True
        verbose_name = 'Data Interpretation Question'

    objects = DIManager()

    def save(self, *args, **kwargs):
        self.category, created = Category.objects.get_or_create(name='DI')
        self.frequency = 0
        super(DIQuestion, self).save(*args, **kwargs)


class RCQuestion(MultiQuestion):
    class Meta:
        proxy = True
        verbose_name = 'Reading Comprehension Question'

    objects = RCManager()

    def save(self, *args, **kwargs):
        self.category, created = Category.objects.get_or_create(name='RC')
        self.frequency = 0
        super(RCQuestion, self).save(*args, **kwargs)


class MathQuestion(CategorizedQuestion):
    class Meta:
        proxy = True
        verbose_name = 'Mathematics Question'

    objects = MathManager()

    def save(self, *args, **kwargs):
        self.category, created = Category.objects.get_or_create(name='MATH')
        self.frequency = 0
        super(MathQuestion, self).save(*args, **kwargs)


class LogicalQuestion(CategorizedQuestion):
    class Meta:
        proxy = True
        verbose_name = 'Logical Reasoning Question'

    objects = LogicalManager()

    def save(self, *args, **kwargs):
        self.category, created = Category.objects.get_or_create(name='LOGICAL')
        self.frequency = 0
        super(LogicalQuestion, self).save(*args, **kwargs)
