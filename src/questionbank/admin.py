from django.contrib import admin
from django import forms

from questionbank.models import MathQuestion, LogicalQuestion
from questionbank.models import RCQuestion, DIQuestion, SubQuestion, SubCategory
from questionbank.models import CategorizedQuestion, MultiQuestion, Option
from questionbank.widgets import ForeignCKEditor

from ckeditor.widgets import CKEditor
from relatedwidget import RelatedWidgetWrapperBase


class QuestionForm(forms.ModelForm):
    class Meta:
        model = CategorizedQuestion
        exclude = ['category', 'frequency']

    question = forms.CharField(widget=CKEditor())
    correct = forms.CharField(widget=ForeignCKEditor(ckeditor_config='options'))
    option1 = forms.CharField(widget=ForeignCKEditor(ckeditor_config='options'))
    option2 = forms.CharField(widget=ForeignCKEditor(ckeditor_config='options'))
    option3 = forms.CharField(widget=ForeignCKEditor(ckeditor_config='options'))

    def clean_correct(self, *args, **kwargs):
        correct = self.cleaned_data.get('correct', '')
        option, created = Option.objects.get_or_create(answer=correct)
        option.save()
        return option

    def clean_option1(self, *args, **kwargs):
        correct = self.cleaned_data.get('option1', '')
        option, created = Option.objects.get_or_create(answer=correct)
        option.save()
        return option

    def clean_option2(self, *args, **kwargs):
        correct = self.cleaned_data.get('option2', '')
        option, created = Option.objects.get_or_create(answer=correct)
        option.save()
        return option

    def clean_option3(self, *args, **kwargs):
        correct = self.cleaned_data.get('option3', '')
        option, created = Option.objects.get_or_create(answer=correct)
        option.save()
        return option


class QuestionAdmin(RelatedWidgetWrapperBase, admin.ModelAdmin):
    form = QuestionForm
    fieldsets = (
        (None, {
            'fields': ('question','subcategory'),
        }),
        (None, {
            'fields': [('correct', 'option1')],
        }),
        (None, {
            'fields': [('option2', 'option3')],
        }),
    )
    list_filter = ('subcategory',)    


class SubQuestionInline(admin.StackedInline):
    model = SubQuestion
    form = QuestionForm
    fieldsets = (
        (None, {
            'fields': ('question',),
        }),
        (None, {
            'fields': [('correct', 'option1')],
        }),
        (None, {
            'fields': [('option2', 'option3')],
        }),
    )
    max_num = 5
    extra = 5


class MultiQuestionForm(forms.ModelForm):
    class Meta:
        model = MultiQuestion
        exclude = ['category', 'frequency']

    data = forms.CharField(widget=CKEditor())


class MultiQuestionAdmin(RelatedWidgetWrapperBase, admin.ModelAdmin):
    form = MultiQuestionForm
    inlines = (SubQuestionInline,)
    list_filter = ('subcategory',)   


admin.site.register(MathQuestion, QuestionAdmin)
admin.site.register(LogicalQuestion, QuestionAdmin)
admin.site.register(RCQuestion, MultiQuestionAdmin)
admin.site.register(DIQuestion, MultiQuestionAdmin)
admin.site.register(SubCategory)
